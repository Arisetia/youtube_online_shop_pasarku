import React, { Component } from 'react';
import Styles from '../components/styles/global.module.css';
import { FaCity } from "react-icons/fa";
import { Link } from 'react-router-dom';

import config from '../config/config';
import axios from "axios";

class Chekout extends Component {


    constructor(props) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            address: '',
            email: '',
            phone: '',
            counter: 0
        }

        this.submitForm = this.submitForm.bind(this);
        this.handleFirstName = this.handleFirstName.bind(this)
        this.handleLastName = this.handleLastName.bind(this)
        this.handleAddress = this.handleAddress.bind(this)
        this.handleEmail = this.handleEmail.bind(this)
        this.handlePhone = this.handlePhone.bind(this)
    }

    submitForm(event) {
        event.preventDefault();
        const sessionId = localStorage.getItem('cartId');
        axios.post(config.ROOT_URL + 'frontend/checkout?session_id=' + sessionId, this.state)
            .then(result => {
                if (result.data.code === 200) {
                    window.location.href = 'transaksi/' + result.data.data.trs_number
                }
            }).catch(err => {
                alert(err.message)
            })
    }

    handleFirstName(event) {
        this.setState({ first_name: event.target.value });
    }
    handleLastName(event) {
        this.setState({ last_name: event.target.value });
    }
    handleEmail(event) {
        this.setState({ email: event.target.value });
    }
    handleAddress(event) {
        this.setState({ address: event.target.value });
    }
    handlePhone(event) {
        this.setState({ phone: event.target.value });
    }

    componentDidMount() {
        const sessionId = localStorage.getItem('cartId');
        axios.get(config.ROOT_URL + 'frontend/keranjang?session_id=' + sessionId)
            .then(result => {
                if (result.data.code === 200) {
                    const counter = result.data.data.length;
                    this.setState({ counter })
                }

            })
    }

    render() {
        return (
            <React.Fragment>
                <div className={Styles.row}>
                    <div className={Styles.container}>
                        {this.state.counter > 0 ?

                            <form className={Styles.form} onSubmit={this.submitForm}>
                                <h2 className={Styles.color_primary}>Isi form untuk Metode pembayaran</h2>

                                <div className={Styles.form_2_grid}>
                                    <div className={Styles.grid_form_2}>
                                        <input type="text" className={Styles.input_style} placeholder="Nama Depan" onKeyUp={this.handleFirstName} />
                                        <input type="email" className={Styles.input_style} placeholder="Email" onKeyUp={this.handleEmail} />
                                        <div className={Styles.payment_method}>
                                            <h4 className={Styles.color_primary}>Metode Pembayaran</h4>
                                            <div className="">
                                                <FaCity /> <span>Bank Transfer</span>
                                            </div>
                                            <p>Bank BCA an. Indocreativ No. 12347654</p>
                                        </div>
                                    </div>
                                    <div className={Styles.grid_form_2}>
                                        <input type="text" className={Styles.input_style} placeholder="Nama Belakang" onKeyUp={this.handleLastName} />
                                        <textarea className={`${Styles.input_style} ${Styles.input_textarea}`} placeholder="Alamat lengkap" onKeyUp={this.handleAddress}></textarea>
                                        <input type="text" placeholder="Telp" className={Styles.input_style} onKeyUp={this.handlePhone} />
                                    </div>

                                </div>
                                <button type="submit" className={Styles.btn_primary}>Proses Pembayaran</button>
                            </form>

                            :
                            <div className={Styles.text_center}>
                                <h2>Kamu belum memiliki keranjang !</h2>
                                <Link to="/" className={Styles.btn_primary}>Kembali ke Home</Link>
                            </div>
                        }

                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Chekout;