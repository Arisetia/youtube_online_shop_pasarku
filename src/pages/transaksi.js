import React, { Component } from 'react';
import Styles from '../components/styles/global.module.css';
import { FaCheckSquare } from 'react-icons/fa';
import { Link } from 'react-router-dom';

class Transaksi extends Component {

    constructor(props) {
        super(props)

        this.state = {
            trs_number: ''
        }
    }

    componentDidMount() {
        const trsNumber = this.props.match.params.number;
        this.setState({ trs_number: trsNumber })
    }
    render() {
        return (
            <React.Fragment>
                <div className={Styles.row}>
                    <div className={Styles.container}>
                        <div className={`${Styles.transaksi_container} ${Styles.text_center}`}>
                            <FaCheckSquare />
                            <div className={Styles.space}></div>
                            <h2>Pembayaran Sukses</h2>
                            <p>Dengan No Transaksi: {this.state.trs_number}</p>
                            <div className={Styles.space}></div>
                            <Link to="/" className={Styles.btn_primary}>Kembali ke Home</Link>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Transaksi;