import React, { Component } from 'react';
import { Link } from "react-router-dom";
import config from '../config/config';
import axios from 'axios';
import formatRupiah from '../libs/formatRupiah';
import { FaShippingFast, FaFunnelDollar, FaUserShield } from "react-icons/fa";
import Styles from '../components/styles/global.module.css';
import ProdukItem from "../components/produk_item";
import image_1 from "../assets/image_1.png";


class Homepage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            product: [],
            keyword: ''
        }

        this.getInputAction = this.getInputAction.bind(this);
        this.getSearchAction = this.getSearchAction.bind(this);
    }


    getInputAction(event) {
        console.log(event.target.value)
        this.setState({ keyword: event.target.value });
    }

    getSearchAction() {
        window.location.href = 'produk?keyword=' + this.state.keyword;
    }

    componentDidMount() {
        axios.get(config.ROOT_URL + 'frontend/produkHome')
            .then(result => {
                console.log('PRODUCT >> ', result)
                const product = result.data.data;
                this.setState({ product });
            });
    }

    render() {
        return (
            <React.Fragment>
                <div className={`${Styles.wrapper} ${Styles.banner_1}`} ></div>
                <div className={Styles.wrapper}>
                    <div className={Styles.row}>
                        <div className={Styles.card}>
                            <div className={Styles.input_group_1}>
                                <input type="search" onMouseOut={this.getInputAction} placeholder="Cari Produk" />
                                <button onClick={this.getSearchAction}>Cari</button>
                            </div>

                        </div>
                    </div>
                    <div className={Styles.row}>
                        <div className={Styles.grid_produk}>
                            {this.state.product.map(product => <ProdukItem key={product.id} image={config.ROOT_URL + 'public/' + product.image} title={product.title} price={formatRupiah(product.price)} action={product.url} />)}
                        </div>
                    </div>
                    <div className={`${Styles.row} ${Styles.bg_primary} ${Styles.custom_banner}`}>
                        <div className={Styles.grid_2}>
                            <div className={Styles.banner_link}>
                                <h1>Adidas Men Running Sneakers</h1>
                                <p>Performance and design. Taken right to the edge.</p>
                                <Link to='/'>SHOP NOW</Link>
                            </div>
                            <div>
                                <img src={image_1} className={Styles.image_custom_banner} alt="Banner 1" />
                            </div>
                        </div>
                    </div>
                    <div className={`${Styles.row}`} >
                        <div className={Styles.grid_3}>
                            <div>

                                <FaShippingFast />
                                <h4>FREE SHIPPING</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                            </div>
                            <div>

                                <FaFunnelDollar />
                                <h4>100% REFUND</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                            </div>
                            <div>

                                <FaUserShield />
                                <h4>SUPPORT 24/7</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Homepage;