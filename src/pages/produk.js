import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { FaShippingFast, FaFunnelDollar, FaUserShield } from "react-icons/fa";
import Styles from '../components/styles/global.module.css';
import ProdukItem from "../components/produk_item";
import config from '../config/config'
import axios from 'axios';

import createBrowserHistory from 'history/createBrowserHistory';

const history = createBrowserHistory({ forceRefresh: false });

class Produk extends Component {

    constructor(props) {
        super(props);

        this.state = {
            products: [],
            keyword: ''
        }

        this.getInputAction = this.getInputAction.bind(this);
        this.getSearchAction = this.getSearchAction.bind(this);
        // this.getDefaultKeyword = this.getDefaultKeyword.bind(this);
    }


    getInputAction(event) {
        console.log(event.target.value)
        this.setState({ keyword: event.target.value });
    }

    getSearchAction() {
        if (this.state.keyword === '') {
            history.push('produk');
        } else {
            history.push('produk?keyword=' + this.state.keyword);
        }

        this.getProduct();
    }

    // getDefaultKeyword() {

    //     if (this.props.location.search != null) {
    //         const defaultKeyword = ;
    //         return defaultKeyword[1];
    //     } else {
    //         return '';
    //     }
    // }

    async getProduct() {
        let getAxiosResponse = null;
        try {
            let url = '?keyword=' + this.state.keyword

            if (this.state.keyword === '') {
                url = '';
            }

            if (this.props.location.search != "") {
                url = this.props.location.search;
            }
            getAxiosResponse = await axios.get(config.ROOT_URL + 'frontend/produkPage' + url)
        } catch (err) {
            if (err.response.status === 404) {
                console.log(err.response.status)
            } else {
                alert(err.response)
            }
        } finally {

            if (getAxiosResponse === null) {
                this.setState({ products: [] })
            } else {
                const products = getAxiosResponse.data.data;
                this.setState({ products })
            }
        }


    }

    componentDidMount() {
        this.getProduct();
    }

    render() {
        return (
            <React.Fragment>
                <div className={Styles.wrapper}>
                    <div className={Styles.row}>
                        <div className={Styles.card}>
                            <div className={Styles.input_group_1}>
                                <input type="search" value={this.props.location.search != null ? this.props.location.search.split('=')[1] : ''} placeholder="Cari Produk" onMouseOut={this.getInputAction} />
                                <button onClick={this.getSearchAction}>Cari</button>
                            </div>

                        </div>
                    </div>
                    <div className={Styles.row}>
                        <div className={Styles.grid_produk}>
                            {this.state.products.length > 0 ? this.state.products.map((item, index) => {
                                return <ProdukItem key={index} image={config.ROOT_URL + 'public/' + item.image} title={item.title} price={item.price} action={item.url} />
                            }) :
                                <div className={Styles.error_404}>
                                    Tidak ada produk untuk di tampilkan!
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Produk;