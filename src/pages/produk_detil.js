import React, { Component } from 'react';
import config from '../config/config';
import axios from 'axios';
import Styles from '../components/styles/global.module.css';
import { FaCartPlus } from "react-icons/fa";
import formatRupiah from "../libs/formatRupiah";

class Produk_detil extends Component {

    constructor(props) {
        super(props);
        this.state = {
            product_item: {},
            qty: 1
        }


        this.handleMinus = this.handleMinus.bind(this);
        this.handlePlus = this.handlePlus.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }

    componentDidMount() {
        const url = this.props.match.params.url;
        axios.get(config.ROOT_URL + 'frontend/produkDetil/' + url)
            .then(result => {

                console.log("PRODUCT DETIL  >> ", result)
                const product_item = {
                    id: result.data.data.id,
                    title: result.data.data.title,
                    description: result.data.data.description,
                    full_description: result.data.data.full_description,
                    price: formatRupiah(result.data.data.price),
                    image: result.data.data.image,
                    url: result.data.data.url,
                    createdAt: result.data.data.createdAt,
                    kategori: result.data.data.kategori.name
                };

                this.setState({ product_item })
            })
    }

    handlePlus() {
        this.setState(prevState => {
            return { qty: prevState.qty + 1 }
        })
    }

    handleMinus() {
        this.setState(prevState => {
            if (prevState.qty <= 1) {
                alert('Minimal pembelian adalah 1 item');
                return false;
            } else {
                return { qty: prevState.qty - 1 }
            }
        })
    }

    addToCart() {
        const data = {
            produk_id: this.state.product_item.id,
            qty: this.state.qty,
            session_id: localStorage.getItem('cartId')
        }

        console.log("DATA >> ", data)
        axios.post(config.ROOT_URL + 'frontend/keranjang', { data })
            .then(result => {
                alert(result.data.message);
                this.props.setCounter();
            })

    }

    render() {
        return (
            <React.Fragment>
                <div className={Styles.row}>
                    <div className={Styles.container_detil_produk}>
                        <div className={Styles.detil_produk} >
                            <div className={Styles.item}>
                                <img src={config.ROOT_URL + 'public/' + this.state.product_item.image} alt="Gambar produk" />
                            </div>
                            <div className={Styles.item}>
                                <div>
                                    <h2>{this.state.product_item.title}</h2>
                                    <h4>{this.state.product_item.price}</h4>
                                    <hr />
                                    <p>Kategori: {this.state.product_item.kategori}</p>
                                    <p>Deskripsi Singkat: {this.state.product_item.description}</p>
                                    <hr />
                                    <div className={Styles.input_group_2}>
                                        <button onClick={this.handleMinus}>-</button>
                                        <input type="number" value={this.state.qty} />
                                        <button onClick={this.handlePlus}>+</button>
                                    </div>

                                    <button onClick={this.addToCart} className={Styles.tambah_keranjang}><FaCartPlus /> Tambah ke keranjang</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={Styles.row}>
                    <div className={Styles.container_detil_produk}>
                        <div className={Styles.deskripsi_produk} >
                            <h2>DESKRIPSI</h2>
                            <hr />
                            <p>
                                {this.state.product_item.full_description}
                            </p>
                        </div>

                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Produk_detil;