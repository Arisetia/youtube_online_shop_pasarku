import React, { Component } from 'react';
import Styles from '../components/styles/global.module.css';


class Kontak extends Component {
    render() {
        return (
            <React.Fragment>
                <div className={Styles.row}>
                    <div className={Styles.container}>
                        <div className={`${Styles.form_2_grid} ${Styles.shadow}`}>
                            <div className={`${Styles.grid_form_2} ${Styles.minPadding}`}>
                                <div className={Styles.kontak_left}>
                                    <h2>Kontak Kami</h2>
                                    <p>Email : admin@mail.com</p>
                                    <p>Telp : (021) 123 1234</p>
                                    <p>Alamat kami : Jl Pajajaran Depok 1 Jawa barat 16515</p>
                                </div>
                            </div>
                            <div className={Styles.grid_form_2}>
                                <div class="elfsight-app-d7b4bf2b-40e6-4d22-9c84-b6a2e442ab6a"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Kontak;