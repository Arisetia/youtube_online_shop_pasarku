import React, { Component } from 'react';
import Styles from '../components/styles/global.module.css';
import { FiX } from "react-icons/fi";
import { Link } from 'react-router-dom';
import config from "../config/config";
import axios from "axios";
import formatRupiah from "../libs/formatRupiah";


class Keranjang extends Component {

    constructor(props) {
        super(props)

        this.state = {
            product: []
        }

        this.deleteCart = this.deleteCart.bind(this);
    }

    componentDidMount() {

        this.getCart();
    }

    getCart = () => {
        const sessionId = localStorage.getItem('cartId');
        axios.get(config.ROOT_URL + 'frontend/keranjang?session_id=' + sessionId)
            .then(result => {
                if (result.data.code === 200) {
                    const product = result.data.data;

                    console.log('DATA PRODUCT > ', product)
                    this.setState({ product })
                } else {
                    console.log('Data Kosong > ', result)
                }

            })
    }

    deleteCart = (id) => {
        // console.log("DEL >> ", id)
        axios.delete(config.ROOT_URL + 'frontend/keranjang/' + id).then(result => {
            this.getCart();
            console.log('Delete Data > ', result)
            alert(result.data.message)
        })
    }
    render() {
        let totalProduk = 0;
        return (
            <React.Fragment>
                <div className={Styles.row}>
                    <div className={Styles.keranjang_container}>
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Produk</th>
                                    <th>@Harga</th>
                                    <th>Qty</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>

                                {
                                    this.state.product.length > 0 ?
                                        this.state.product.map((item, index) => {

                                            totalProduk += item.produk.price * item.qty;
                                            return (
                                                <tr key={item.id}>
                                                    <td className={Styles.text_center}>
                                                        <button onClick={() => this.deleteCart(item.id)} className={Styles.hapus_keranjang}><FiX /> </button>
                                                    </td>
                                                    <td>
                                                        <div className={Styles.item_keranjang}>
                                                            <img src={config.ROOT_URL + 'public/' + item.produk.image} />
                                                            <h4>{item.produk.title}</h4>
                                                        </div>
                                                    </td>
                                                    <td className={Styles.text_right}>{formatRupiah(item.produk.price, false)}</td>
                                                    <td className={Styles.text_center}>{item.qty}</td>
                                                    <td className={Styles.text_right}>{formatRupiah(item.produk.price * item.qty, false)}</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        <tr>
                                            <td colSpan="5" className={Styles.text_center}>Belum ada data di keranjang!</td>
                                        </tr>
                                }

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className={Styles.row}>
                    <div className={Styles.total_keranjang}>
                        <div>
                            <table className={Styles.table_total_keranjang}>
                                <tr>
                                    <td>TOTAL</td>
                                    <td className={Styles.text_right}>{formatRupiah(totalProduk)}</td>
                                </tr>
                                <tr>
                                    <td colSpan="2">
                                        <Link to='chekout' >Bayar Sekarang</Link>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Keranjang;