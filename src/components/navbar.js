import React, { Component } from "react";
import { Link } from "react-router-dom";
import Styles from "./styles/navbar.module.css";
import Logo from '../assets/logo.png';
import { FaShoppingCart } from "react-icons/fa";



class Navbar extends Component {
    constructor(props) {
        super(props);

        console.log(this.props.getCounter)
    }

    render() {
        return (
            <div className={Styles.header}>
                <div className={Styles.container}>
                    <div>
                        <img className={Styles.logo} src={Logo} alt="Logo" />
                    </div>
                    <ul className={Styles.topmenu}>
                        <li><Link to="/">Homepage</Link></li>
                        <li><Link to="/produk">Produk</Link></li>
                        <li><Link to="/kontak">Kontak</Link></li>
                        <li><Link to="/tentang">Tentang</Link></li>
                    </ul>
                    <div className={Styles.cart}>
                        <Link to="/keranjang"><FaShoppingCart /></Link>
                        {this.props.getCounter > 0 ? <span className={Styles.badgeCart}>{this.props.getCounter}</span> : ''}
                    </div>
                </div>
            </div>

        )
    }
}

export default Navbar;