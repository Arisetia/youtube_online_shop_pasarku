import React from "react";
import Styles from "../components/styles/global.module.css";
import footerLogo from "../assets/logo_light.png";
const Footer = () => {
    return (
        <div className={`${Styles.row} ${Styles.bg_primary} ${Styles.footer}`}>
            <img src={footerLogo} alt="logo" />
            <p>@ Copyright Pasarku 2021</p>
        </div>
    )

}


export default Footer;